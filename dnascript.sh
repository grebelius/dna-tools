#!/usr/local/bin/bash

###### Helptext ######
if [[ $# -eq 0 ]] ; then
    echo "              $0 filepath"
    echo "Exempelvis:   $0 ./example.txt"
    exit 0
fi

###### Constants ######
NUMBEROFLINESPERFILE=11166
OUTPUTDIR=split_files
PWD=$(pwd)

###### Variables ######
PASSEDFILEPATH=$1
DIR=$(dirname "${PASSEDFILEPATH}")
NAME=$(basename "${PASSEDFILEPATH}")
OUTPUTPATH="$DIR/$OUTPUTDIR"
BACKUPFILETOWORKFROM=$OUTPUTPATH/$NAME

###### Create output directory and copy file into it ######
echo ""
echo "  => Arbetsmapp: \"$OUTPUTDIR\""
if [ ! -d $OUTPUTPATH ]; then
  mkdir $OUTPUTPATH
fi

echo "     Backuppfil: \"$NAME.bak\""
if [ ! -f $BACKUPFILETOWORKFROM.bak ]; then
  cp $PASSEDFILEPATH $BACKUPFILETOWORKFROM.bak
fi

###### Insert spaces ######
echo "     Fil med mellanslag mellan bokstäverna: \"$NAME.spaces\""
if [ ! -f $BACKUPFILETOWORKFROM.spaces ]; then
  gawk '$1=$1' FS= OFS=" " < $BACKUPFILETOWORKFROM.bak > $BACKUPFILETOWORKFROM.spaces
fi

###### Split file into chunks with $NUMBEROFLINES lines in each, plus the remainder in the last file ######
#(cd $OUTPUTPATH && gsplit --numeric-suffixes=1 -a3 --additional-suffix=".txt" -l$NUMBEROFLINESPERFILE "$NAME.spaces" "${NAME%.*}-")
antalfiler=$(find $OUTPUTPATH -type f -maxdepth 1 -name '*.txt' | wc -l)
echo "  => Skapar"$antalfiler" filer med $NUMBEROFLINESPERFILE rader var"

###### For each file, replace newline with space ######
echo -n " "
shopt -s nullglob
i=0
for fname in $OUTPUTPATH/*.txt ; do
  ((i+=1))
  echo -ne "\r     Konverterar fil $i"
  mv $fname $fname.tmp
  tr '\n' ' ' < $fname.tmp > $fname
  rm $fname.tmp
done
echo -e "\r     Färdigt                       "

echo ""
echo "  => $i filer utan radbrytningar och med mellanslag mellan bokstäverna skapades"
